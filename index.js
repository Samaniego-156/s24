
// Identify and prepare the components/ingredeitns needed in order to execute the task
// http (hypertext transfer protocol)
// http -> is built in module of NodeJS. That will allow us to establish a connection and allow us to transfer data over http
// use a function called require() in order to acquire the desired module. repackage the module and store it inside a new variable.

let http = require ('http');

	// http will provide us with all the components needed to establish a server
	// createServer() -> will allow us to create an http server
	// provide/instruct the server what to do
		// ex: print a simple message
	// Identify and describe a location where the connection will happen in order to provide a proper medium for both parties

let port = 3000;

	// listen() -> bind and listen to a specific port whenever it's being accessed by your computer.

	// identify a point where we want to terminate the transmission using the end() function
http.createServer(function (request, response){
	response.write('Hello Batch 156!');
	response.end(); // terminate the transmission
}).listen(port);

	// create a response in the terminal to confirm if a connection has been successfully established.
		// 1. we can give a confirmation to the client that a connection is working
		// 2. we can specify what address/location the connection was established.
	// we will use template literals in order to utilize placeholders
console.log(`Server is running with nodemon on port: ${3000}`);

	// What can we do for us to be able to automatically hotfix a system without restarting a server?
		// 1. initialize an npm into your local project
			// Syntax: npm init
			// shortcut syntax: npm init -y (yes)

		// package.json -> "The Heart of any Node Projects". It records all the important metadata about the project, 
		// (libraries, packages, functions) that makes up the system.

		// 2. install 'nodemon' into our project.
			// Practice Skills:
				// install (i)
					// npm install <dependency>
				// uninstall (un)
					// npm uninstall <dependency>
				// installing multiple dependencies
					// npm i <list of dependencies>
						// add nodemon and express

		// 3. run your app using the nodemon utility engine.
			// make sure that nodemon is recognized by your file system structure

			// npm install -g nodemon (-g - global)
				// the library is being installed "globally" into your machine's file system so that the program will become recognizable across
				// your file system structure
				// (*YOU ONLY HAVE TO EXECUTE THIS COMMAND ONCE*)

			// identify entry point ("main" in package.json)

		// nodemon utility -> is a CLI utility tool, it's task is to wrap your node js app and watch for any changes
		// in the file system and automatically restarts/hotfix the process. (nodemon <filename>)

		// create your personalized script to execute the app using the nodemon utility.
			// register a new command to start the app.
			// start -> common execute script for Node apps.
			// others -> needs to use "run" + "others" to execute for Node apps
			

// [SEGWAY] Register sublime text 3 as part of your environment variables.

	// 1.Locate in your PC where Sublime Text was installed.
	// 2. Copy path where sublime text is installed (C:\Program Files\Sublime Text)
	// 3. Locate the env variables in your system
	// 4. Insert the path of Sublime Text under 'Path' of the system env.